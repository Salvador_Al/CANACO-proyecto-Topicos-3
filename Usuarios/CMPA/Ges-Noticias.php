<?php
session_start();
require_once('../conexion.php');
$objBaseDatos=new ConexionSql('127.0.0.1:3355','root','','canaco');
$objBaseDatos->conectar(); 
?>
<title>Canaco: Administrador</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <link href="open-iconic-master/font/css/open-iconic-bootstrap.css" rel="stylesheet">

<body style="background-color: #CBC6C4;">
    <br>
    <div class="row bg-info shadow">
        <div class=" col-2">
            <div class="text-center pt-2" style="background-color: #D4700B;">
                <img src="img/perfil desconocido 2.jpg" alt="imgProfile" style="width: 5rem; height: 5rem;"
                    class="rounded-circle shadow">
                <br>
                <label for="Nombre">¡Bienvenido!</label>
                <br>
                <label class="h3" style="color: #CBC6C4"><?php $id=$_SESSION['id']; $consulta=$objBaseDatos->ConsultaSql("select nickname from Administrador where id_admin=$id");  echo "".$consulta[0][0];
                ?></label>
            </div>
        </div>
        <div class="col-7 pr-3 mr-4"></div>
        <div class="col-2 d-flex flex-row bd-highlight">
        <br>
        <a class="text-white ml-3" href="modAdmin1.php"><span class="oi oi-cog ml-3"></span> Editar Perfil</a>
        <br>
        <a class="text-white ml-3" href="Admin.php"><span class="oi oi-home ml-3"></span> Pag. Principal</a>
        <form name="cierre" method="post" action="CerrarSesion.php">
        <?php 

           echo "<button class='btn btn-danger rounded-1 border-top ml-3' role='button' type='submit' name='ses' value='0'><span class='oi oi-circle-x'></span>Cerrar Sesión</button>";

           
       ?>      
        </form>
        </div>
        
    </div>
    <br>
    <div class="row">
        <div class="col-2">
            <div class="nav flex-column nav-pills nav-tabs text-center bg-white shadow" id="v-pills-tab" role="tablist"
                aria-orientation="vertical">
                <a class="nav-link active rounded-0 border border-right-0" id="v-pills-home-tab" data-toggle="pill"
                    href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true"><span
                        class="oi oi-document"></span> Tramites</a>
                <a class="nav-link border rounded-0 border-right-0" id="v-pills-profile-tab" data-toggle="pill"
                    href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false"><span
                        class="oi oi-justify-left"></span> Gestion de Noticias</a>
                <a class="nav-link border rounded-0 border-right-0" id="v-pills-messages-tab" data-toggle="pill"
                    href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false"><span
                        class="oi oi-person"></span> Usuarios</a>
                <a class="nav-link border rounded-0 border-right-0" id="v-pills-settings-tab" data-toggle="pill"
                    href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"><span
                        class="oi oi-people"></span> Sociedades</a>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="tab-content bg-white shadow" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel"aria-labelledby="v-pills-home-tab">
                    <?php require_once("CMPA/Tramites.php"); ?>
                </div>
                <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                <div class="row">
                <div class="col-4"></div>
                <div class="col-3">
                    <h2 class="text-center">Gestor De Noticias</h2>
                    <hr>
                </div>
                <div class="col-4"></div>
            </div>
            <div class="row">
                <div class="col-3"></div>
                <div class="col-4">
                    <input type="text" class="form-control mr-2" placeholder="Buscar">
                </div>
                <button type="button" class="btn btn-outline-secondary"><span class="oi oi-magnifying-glass"></span></button>
                <div class="col-3"></div>
            </div>
            <br>
            <div class="row">
                <div class="col-11 ml-lg-5">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr class="text-center">
                                <th scope="col">#</th>
                                <th scope="col">Noticia</th>
                                <th scope="col">Fecha de modificacion</th>
                                <th scope="col">Publicado por:</th>
                                <th scope="col">Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cadm=$objBaseDatos->ConsultaSql("select * from detadminnot");
                            $cemp=$objBaseDatos->ConsultaSql("select * from detempnot");
                            $csoc=$objBaseDatos->ConsultaSql("select * from detsocnot");
                            $num=1;
                            if($cadm==null){
                                echo "<td colspan='5' class='text-center text-danger h3'>No Se Encontraron Resultados</td>";
                            }
                            else{
                                $notadm=array();
                                $cladm=array();
                                foreach ($cadm as $admin) {

                                    $notadm[]=$admin[1];
                                    $cladm[]=$admin[0];
                                }
                                for ($x=0; $x<count($notadm);$x++){
                                    $ob=new ConexionSql('localhost','root','','Canaco');
                                    $ob->conectar();
                                    $con=$ob->ConsultaSql("select * from noticias where id_not=".$notadm[$x]."");
                                    $ad=$ob->ConsultaSql("select * from administrador where id_admin=".$cladm[$x]."");
                                    echo "<tr><td class='text-center'>$num</td><td class='text-center'>".$con[0][1]."</td><td class='text-center'>".$con[0][4]."</td><td class='text-center'>".$ad[0][1]."</td>
                                        <td class='row p-0 m-0 justify-content-center'>
                                        <form action='CMPA/Ges_Noticias' method='post' class='mt-2'>
                                            <button class='btn btn-warning mr-2' name='accion' value='1'>Modificar</button>
                                        </form>
                                        <form action='CMPA/Ges-Noticias.php' method='post' class='mt-2'>
                                            <button class='btn btn-danger' name='accion' value='2'>Eliminar   </button>
                                        </form>
                                        </td></tr>";
                                    $num++;
                                }
                            } 
                            
                            ?>
                            
                            
        
                        </tbody>
                    </table>
                </div>
        
            </div>
                </div>
                <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                <?php require_once("CMPA/Usuarios.php"); ?>
                </div>
                <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                <?php require_once("CMPA/Sociedad.php"); ?>
                </div>
            </div>
        </div>
    </div>
</body>

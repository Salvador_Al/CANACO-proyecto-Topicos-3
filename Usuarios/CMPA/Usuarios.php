<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <link href="open-iconic-master/font/css/open-iconic-bootstrap.css" rel="stylesheet">
</head>

<body>
    <div class="row">
        <div class="col-4"></div>
        <div class="col-3">
            <h2 class="text-center">Usuarios</h2>
            <hr>
        </div>
        <div class="col-4"></div>
    </div>
    <div class="row">
        <div class="col-3"></div>
        <div class="form-group row mr-3">
            <label class="col-form-label mr-2" for="exampleFormControlSelect1">Tipo de Usuario:</label>
            <div>
                <select class="form-control" id="exampleFormControlSelect1">
                    <option>Todos</option>
                    <option>Normal</option>
                    <option>Empendedor</option>
                    <option>Empresario</option>
                </select>
            </div>
        </div>
        <div class="col-3 mb-3">
            <input type="text" class="form-control mr-2" placeholder="Buscar">
        </div>
        <button type="button" class="btn btn-outline-secondary mb-3"><span class="oi oi-magnifying-glass"></span></button>
    </div>
    <br>
    <div class="row">
        <div class="col-11 ml-lg-5">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th scope="col">Nickname</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido Pat.</th>
                        <th scope="col">Tipo</th>
                        <th scope="col">Telefono</th>
                        <th scope="col">Editar</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>

    </div>
</body>

</html>
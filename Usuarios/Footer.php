<!-- Footer -->
<footer class="page-footer font-small bg-info">
    <div class="container text-center text-md-left mt-5 pt-5">
        <div class="row mt-3">
            <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
                <h6 class="text-uppercase font-weight-bold">Sobre la pagina</h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 140px;">
                <p>Esta pagina web se realiza como proyecto de la materia Topicos III, ademas de demostrar el diseño, se va demostrando el avance de desarrollo de ella misma
                </p>
            </div>
            <div class="col-md-3 mx-auto mb-4">
                <h6 class="text-uppercase font-weight-bold">Redes Sociales</h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 130px;">
                <p>
                    <a href="https://www.facebook.com/CanacoSahuayo" style="color:#000000;"><img src="../CanacoIMG/icon/icon-facebook.png" class="m-sm-1" style="width: 30px; height: 30px;" alt="Facebook">Canaco Sahuayo</a>
                </p>
                <p>
                    <a href="https://twitter.com/CanacoSahuayo" style="color:#000000;"><img src="../CanacoIMG/icon/icon-twitter.png" class="m-sm-1" style="width: 30px; height: 30px;" alt="twitter">@CanacoSahuayo</a>
                </p>
                <p>
                    <a href="https://www.youtube.com/channel/UC8JsCEEaOsBkXMG7wtHPVkA" style="color:#000000;"><img src="../CanacoIMG/icon/icon-youtube.png" class="m-sm-1" style="width: 30px; height: 30px;" alt="Youtube">Canaco Sahuayo</a>
                </p>
            </div>
            <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                <h6 class="text-uppercase font-weight-bold">Contacto</h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 100px;">
                <p>
                    20 de Noviembre #55 Col.
                </p>
                <p>
                    canacoservytoursahuayo@hotmail.com
                </p>
                <p>
                    + 353 100 3301
                </p>
                <p>
                    + 353 122 12 58
                </p>
            </div>
        </div>
    </div>
    <hr class="center col-10">
    <div class="footer-copyright text-center py-3">2019 Create by:
        <a>Equipo #4</a>
    </div>
</footer>
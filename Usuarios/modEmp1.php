<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>Canaco</title>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
        <!--Open Iconic-->
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.0/font/css/open-iconic-bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
</head>

<body>
    <header><?php require_once("nav.php"); ?></header>
    <div class="container main-container">
        <br>
        <h2 class="text-info"><span class="oi oi-person"></span> Emprendedor</h2>
        <hr>
        <br>
        <div class="row">
            <div class="col-sm-7 pt-3 bg-info">
                <h4 class="text-white"><span class="oi oi-pencil"></span> Nickname</h4>
                <hr>
                <form action="">
                    <div class="form-group">
                        <input type="text" class="form-control" pattern="[A-Za-z]{20}" id="nikold"
                            placeholder="Nickname Actual">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" pattern="[A-Za-z]{20}" id="niknew"
                            placeholder="Nickname Nuevo">
                    </div>
                </form>


                <br>
                <h4 class="text-white"><span class="oi oi-pencil"></span> Datos Personales</h4>
                <hr>
                <form action="">
                    <div class="form-group">
                        <input type="text" class="form-control" pattern="[A-Za-z]{20}" id="nombre" placeholder="Nombre">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" pattern="[A-Za-z]{20}" id="appat"
                            placeholder="Apellido Paterno">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" pattern="[A-Za-z]{20}" id="apmat"
                            placeholder="Apellido Materno">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" pattern="[0-9]{10}" id="telef" placeholder="Telefono">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" id="email" placeholder="Correo">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="passant" placeholder="Contraseña Anterior">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="passnew" placeholder="Contraseña">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="passconf" placeholder="Confirmar Contraseña">
                    </div>
                    <br>
                    <h4 class="text-white"><span class="oi oi-paperclip"></span> Cambiar Foto de Perfil</h4>
                    <hr>
                    <div class="form-group">
                        <input type="file" class="form-control-file text-white" name="fotoperfil" id="imgperfil">
                    </div>
                </form>
            </div>
            <div class="col-sm-4">
                <div class="card bg-info border-0" style="width: 25rem;">
                    <div class="card-header text-white">
                            <span class="oi oi-person"></span> Datos Actuales
                    </div>
                    <ul class="list-group list-group-flush">
                        <?php 

                        require_once('../conexion.php');
                        $objBaseDatos=new ConexionSql('localhost','root','','Canaco');
                        $objBaseDatos->conectar();
                        switch($_SESSION['tipo']){
                        case 'Emprendedor':
                             $id="id_emp";
                             break;
                        case 'Empresario':
                             $id="id_empres";
                             break;
                        case 'Sociedad':
                             $id="id_soc";
                             break;
                        case 'Usuarios':
                             $id="id_usu";
                             break;
                        case 'Administrador':
                             $id="id-admin";
                        }      
                        $us=$objBaseDatos->ConsultaSql("select * from ".$_SESSION['tipo']." where ".$id."=".$_SESSION['id'].";");
                        ?>
                        <li class="list-group-item" id="numafl"># Afiliado:<h3><?php 
                        echo '   '.$us[0][6].''; ?></h3>
                        </li>
                        <li class="list-group-item" id="nomact">Nombre:<h3><?php 
                         echo '   '.$us[0][3].'' ?></li></h3>
                        <li class="list-group-item" id="appatact">Apellido Paterno:<h3><?php 
                         echo '   '.$us[0][4].'' ?></li></h3></li>
                        <li class="list-group-item" id="apmatact">Apellido Materno:<h3><?php 
                         echo '   '.$us[0][5].'' ?></li></h3></li>
                        <li class="list-group-item" id="telact">Telefono:<h3><?php 
                         echo '   '.$us[0][9].'' ?></li></h3></li>
                        <li class="list-group-item" id="emailact">Correo:<h3><?php 
                         echo '   '.$us[0][10].'' ?></li></h3></li>
                        <li class="list-group-item" id="emailact">RFC:<h3><?php 
                         echo '   '.$us[0][11].'' ?></li></h3></li>
                        <li class="list-group-item" id="emailact">CURP:<h3><?php 
                         echo '   '.$us[0][8].'' ?></li></h3></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <footer><?php require_once("footer.php"); ?></footer>
</body>
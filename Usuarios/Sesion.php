<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


<script type="text/javascript">
	function cambio(valor)
	{
		var m=document.getElementById("msj");
		var i=document.getElementById("input");

		switch(valor){
			case "Empresario":
			m.innerHTML="Nombre de Usuario";
			i.placeholder="Nickname";
			break;
			case "Usuarios":
			m.innerHTML="Nombre de Usuario";
			i.placeholder="Nickname";
			break;
			case "Sociedad":
			m.innerHTML="Numero de Afiliación";
			i.placeholder="Numero";
			break;
			case "Emprendedor":
			m.innerHTML="Nombre de Usuario";
			i.placeholder="Nickname";
			break;
			case "Administrador":
			m.innerHTML="Nombre de Usuario";
			i.placeholder="Nickname";
		}
	}
	function Validar()
	{
		var t=document.getElementById("tipos");
		var n=document.getElementById("input");
		var c=document.getElementById("con");
		if (t.value=="nada") {
			t.style="background-color:Red";
			t.focus();
		}
		else
		    if(n.value==""){
				n.style="background-color:Red";
				n.focus();
			}
			else
				if(c.value==""){
				c.style="background-color:Red";
				c.focus();
				}
				else
					document.getElementById("formulario").submit();			
    }


    function Normalizar(){
    	var t=document.getElementById("tipos");
		var n=document.getElementById("input");
		var c=document.getElementById("con");
		t.style="background-color:none";
		n.style="background-color:none";
		c.style="background-color:none";

    }
	
</script>



<header><?php require_once("Nav.php"); ?></header>


<?php if($_GET)
{
	if($_GET['modal']==1){
		echo " 
		<div class='alert alert-danger text-center' role='alert'>El nickname, Numero de afiliación o contraseña es incorrecto</div>
		";
	}

} 

?>

<div class="container">
	
	
			
		<p class="h2 text-center">Inicio de Sesión</p>

        <form  name="form1" id="formulario" action="Carga.php" method="post">
        	<input type="hidden" name="carga" value="2">
			<div class="d-flex col-md-12 mt-3 mr-0 border-dark">
				<div class="d-flex col-md-6 justify-content-end pr-0">
				<p class="col-md-6 form-control border border-dark text-center" style="background-color:57d2f7;">Seleccione el tipo de cuenta:</p>
				</div>
				<div class=" ml-3 d-flex col-md-6 justify-content-start pl-0">
					<select name="tipo" onchange="cambio(this.value);Normalizar();" class="col-md-4 form-control border-info" id="tipos">
					<option value="nada">Elegir...</option>
					<option value="Administrador">Administrador</option>
					<option value="Usuarios">Normal</option>
					<option value="Emprendedor">Emprendedor</option>
					<option value="Sociedad">Sociedad</option>
					<option value="Empresario">Empresario</option>
				    </select>
				</div>
			</div>
			<div class="d-flex col-md-12 mt-0 mr-0 border-dark">
				<div class="d-flex col-md-6 justify-content-end pr-0">
					<p class="col-md-6 form-control border border-dark text-center" style="background-color:57d2f7;" id="msj">Nombre de Usuario</p>
				</div>
				<div class=" ml-3 d-flex col-md-6 justify-content-start pl-0">
					 <input type="text" aria-label="First name" name="nombre" class="form-control col-md-6" placeholder="Nickname" id="input" onchange="Normalizar();">
					
				</div>
			</div>
			<div class="d-flex col-md-12 mt-0 mr-0 border-dark">
				<div class="d-flex col-md-6 justify-content-end pr-0">
					<p class="col-md-6 form-control border border-dark text-center" style="background-color:57d2f7;">Contraseña</p>
				</div>
				<div class=" ml-3 d-flex col-md-6 justify-content-start pl-0">
					 <input id="con" type="password" aria-label="Password" class="form-control col-md-6" placeholder="Contraseña" name="pass" onchange="Normalizar();">
					
				</div>
			</div>
            <div class="d-flex justify-content-center mt-2">
            	<button type="button" class="btn btn-success btn-lg col-md-3" onclick="Validar();">Iniciar Sesión <img src="../CanacoIMG/icon/login.png" width="30px" height="30px"></button>
            </div>
            <p class="h5 text-center">ó</p>
            <div class="d-flex justify-content-center mt-2">
            	<?php require_once("../login.php"); ?>
            </div>
        
        </form>
   <div class="d-flex justify-content-center mt 0">
   	<img src="../CanacoIMG/icon/icon-Canaco.png" class="border-1 col-8" height="350px">
   	
   </div>







<footer><?php require_once("Footer.php"); ?> </footer>
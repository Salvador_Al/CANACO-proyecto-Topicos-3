<?php
session_start();
require_once('../conexion.php');
$objBaseDatos=new ConexionSql('127.0.0.1:3355','root','','canaco');
$objBaseDatos->conectar(); 
?>
<title>Canaco: Administrador</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <link href="open-iconic-master/font/css/open-iconic-bootstrap.css" rel="stylesheet">

<body style="background-color: #CBC6C4;">
    <br>
    <div class="row bg-info shadow">
        <div class=" col-2">
            <div class="text-center pt-2" style="background-color: #D4700B;">
                <img src="img/perfil desconocido 2.jpg" alt="imgProfile" style="width: 5rem; height: 5rem;"
                    class="rounded-circle shadow">
                <br>
                <label for="Nombre">¡Bienvenido!</label>
                <br>
                <label class="h3" style="color: #CBC6C4"><?php $id=$_SESSION['id']; $consulta=$objBaseDatos->ConsultaSql("select nickname from Administrador where id_admin=$id");  echo "".$consulta[0][0];
                ?></label>
            </div>
        </div>
        <div class="col-7 pr-3 mr-4"></div>
        <div class="col-2 d-flex flex-row bd-highlight">
        <br>
        <a class="text-white ml-3" href="modAdmin1.php"><span class="oi oi-cog ml-3"></span> Editar Perfil</a>
        <br>
        <a class="text-white ml-3" href="Admin.php"><span class="oi oi-home ml-3"></span> Pag. Principal</a>
        <form name="cierre" method="post" action="CerrarSesion.php">
        <?php 

           echo "<button class='btn btn-danger rounded-1 border-top ml-3' role='button' type='submit' name='ses' value='0'><span class='oi oi-circle-x'></span>Cerrar Sesión</button>";

           
       ?>      
        </form>
        </div>
        
    </div>
    <br>
    <div class="row">
        <div class="col-2">
            <div class="nav flex-column nav-pills nav-tabs text-center bg-white shadow" id="v-pills-tab" role="tablist"
                aria-orientation="vertical">
                <a class="nav-link active rounded-0 border border-right-0" id="v-pills-home-tab" data-toggle="pill"
                    href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true"><span
                        class="oi oi-document"></span> Tramites</a>
                <a class="nav-link border rounded-0 border-right-0" id="v-pills-profile-tab" data-toggle="pill"
                    href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false"><span
                        class="oi oi-justify-left"></span> Gestion de Noticias</a>
                <a class="nav-link border rounded-0 border-right-0" id="v-pills-messages-tab" data-toggle="pill"
                    href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false"><span
                        class="oi oi-person"></span> Usuarios</a>
                <a class="nav-link border rounded-0 border-right-0" id="v-pills-settings-tab" data-toggle="pill"
                    href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"><span
                        class="oi oi-people"></span> Sociedades</a>
            </div>
        </div>
        </div>
    </div>
</body>
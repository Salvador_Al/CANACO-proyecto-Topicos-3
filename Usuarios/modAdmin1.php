<?php
session_start();
require_once('../conexion.php');
$objBaseDatos=new ConexionSql('localhost','root','','Canaco');
$objBaseDatos->conectar(); 
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Canaco</title>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
        <!--Open Iconic-->
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.0/font/css/open-iconic-bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
</head>

<body>
    <br>
    <div class="row bg-info shadow">
        <div class=" col-2">
            <div class="text-center pt-2" style="background-color: #D4700B;">
                <img src="img/perfil desconocido 2.jpg" alt="imgProfile" style="width: 5rem; height: 5rem;"
                    class="rounded-circle shadow">
                <br>
                <label for="Nombre">¡Bienvenido!</label>
                <br>
                <label class="h5"><?php $id=$_SESSION['id']; $consulta=$objBaseDatos->ConsultaSql("select nickname from Administrador where id_admin=$id");  echo "".$consulta[0][0];
                ?></label>
            </div>
        </div>
        <div class="col-7 pr-3 mr-4"></div>
        <div class="col-2 d-flex flex-row bd-highlight">
        <br>
        <a class="text-white ml-3" href="modAdmin1.php"><span class="oi oi-cog ml-3"></span> Editar Perfil</a>
        <br>
        <a class="text-white ml-3" href="Admin.php"><span class="oi oi-home ml-3"></span> Pag. Principal</a>
        <form name="cierre" method="post" action="CerrarSesion.php">
        <?php 

           echo "<button class='btn btn-danger rounded-1 border-top ml-3' role='button' type='submit' name='ses' value='0'><span class='oi oi-circle-x'></span>Cerrar Sesión</button>";         
       ?>      
        </form>
        </div>
    </div>
    <div class="container main-container">
        <br>
        <h2 class="text-info"><span class="oi oi-person"></span> Administrador</h2>
        <hr>
        <br>
        <div class="row">
            <div class="col-sm-7 pt-3 bg-info">
                <h4 class="text-white"><span class="oi oi-pencil"></span> Nickname</h4>
                <hr>
                <form action="">
                    <div class="form-group">
                        <input type="text" class="form-control" pattern="[A-Za-z]{20}" id="nikold"
                            placeholder="Nickname Actual">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" pattern="[A-Za-z]{20}" id="niknew"
                            placeholder="Nickname Nuevo">
                    </div>
                </form>
                <br>
                <h4 class="text-white"><span class="oi oi-pencil"></span> Datos Personales</h4>
                <hr>
                <form action="">
                    <div class="form-group">
                        <input type="text" class="form-control" pattern="[A-Za-z]{20}" id="nombre" placeholder="Nombre">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" pattern="[A-Za-z]{20}" id="appat"
                            placeholder="Apellido Paterno">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" pattern="[A-Za-z]{20}" id="apmat"
                            placeholder="Apellido Materno">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="passant" placeholder="Contraseña Anterior">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="passnew" placeholder="Contraseña">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="passconf" placeholder="Confirmar Contraseña">
                    </div>
                    <br>
                    <h4 class="text-white"><span class="oi oi-paperclip"></span> Cambiar Foto de Perfil</h4>
                    <hr>
                    <div class="form-group">
                        <input type="file" class="form-control-file text-white" name="fotoperfil" id="imgperfil">
                    </div>
                </form>
            </div>
            <div class="col-sm-4">
                <div class="card bg-info border-0" style="width: 25rem;">
                    <div class="card-header text-white">
                            <span class="oi oi-person"></span> Datos Actuales
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item" id="nomact">Nombre:</li>
                        <li class="list-group-item" id="appatact">Apellido Paterno:</li>
                        <li class="list-group-item" id="apmatact">Apellido Materno:</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</body>
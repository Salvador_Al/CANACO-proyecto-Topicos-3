<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">

<header><?php require_once("Nav.php"); ?></header>

<?php 
if($_GET)
{
  $reg=$_GET['reg'];
  switch ($reg) {
    case 0:
      echo "<div class='alert alert-danger text-center' role='alert'>La solicitud no se ha podido completar, debido a que las contraseñas no coinciden</div>";
      break;
    case 1:
      echo "<div class='alert alert-danger text-center' role='alert'>La solicitud no se ha podido completar, debido a el nickname o nombre que se intenta registrar ya esta en uso.</div>";
      break;
    
    case 2:
      echo "<div class='alert alert-danger text-center' role='alert'>La solicitud no se ha podido completar, debido a que el nombre que se intenta registrar ya existe</div>";
      break;
    case 3:
      echo "<div class='alert alert-danger text-center' role='alert'>La solicitud no se ha podido completar, debido a que esta curp ya esta dada de alta en nuestros servidores.</div>";
      break;
    case 4:
      echo "<div class='alert alert-danger text-center' role='alert'>No se Aceptaron los Terminos y Condiciones.</div>";
      break;
    case 5:
      echo "<div class='alert alert-danger text-center' role='alert'>RFC ya esta registrado, por favor ingrese otro</div>";
  }
     
}

if ($_POST){
  if($_POST['carga']==1){
    require_once("Login/UsuReg.php");//incluimos la libreria para poder registrar a los usuarios
      //Esta libreria esta en la carpeta de Usuarios/Login/UsuReg
      //cualquien problema con el registro se tiene que ver aqui y en Usureg 
      $objreg=new Reg();// generamos un objeto de la clase de registro
      switch ($_POST['identificador']) {// abrimos otro switch para ver que vamos a registrar

        case 'Usuarios': // registramos Usuarios
          //manden la contraseña ecriptada desde aqui...
          //tengan cuidado con el metodo que mandan llamar...
          //RegUsu para registrar en la tabla Usuarios
          //RegEmp para resgistrar en Emprendedor
          //RegEmpres para Empresarios
          //RegSoc para Sociedad
          // para administrador no se nesesita registro puesto que solo abra un admin
          $res=$objreg->RegUsu($_POST['check'],$_POST['nickname'],md5($_POST['contraseña']),md5($_POST['pass']),$_POST['nom'],$_POST['pat'],$_POST['mat'],$_POST['curp'],$_POST['tel'],$_POST['correo']);
          #la vaidacion de los datos se hace en el RegUsu.php
          if($res==1)
            header("location:Registro.php?reg=1");
          else
            if($res==2)
              header("location:Registro.php?reg=2");
            else
              if($res==3)
                header("location:Registro.php?reg=3");
              else
                if($res==4)
                  header("location:Registro.php?reg=4");
                else
                  if($res==0)
                    header("location: Registro.php?reg=0");
                  else
                    if($res==-1)// si la validacion del registro es -1 entonces procedemos a confirmar el registro
                      header("location:CargaFomularios.php");

          break;
        
        case 'Emprendedor'://registramos emprendedores de la misma manera que usuarios
          $res=$objreg->RegEmp($_POST['check'],$_POST['nickname'],md5($_POST['contraseña']),md5($_POST['pass']),$_POST['nom'],$_POST['pat'],$_POST['mat'],$_POST['curp'],$_POST['tel'],$_POST['correo'],$_POST['rfc']);
          #la vaidacion de los datos se hace en el RegUsu.php
          if($res==1)
            header("location:Registro.php?reg=1");
          else
            if($res==2)
              header("location:Registro.php?reg=2");
            else
              if($res==3)
                header("location:Registro.php?reg=3");
              else
                if($res==4)
                  header("location:Registro.php?reg=4");
                else
                  if($res==5)
                    header("location:Registro.php?reg=5");
                  else
                    if($res==0)
                      header("location: Registro.php?reg=0");
                    else
                      if($res==-1)// si la validacion del registro es -1 entonces procedemos a confirmar el registro
                        header("location:CargaFomularios.php");
          break;
        case 'Empresario'://registramos empresarios de la misma manera que usuarios
           $check=$_POST['mm'];
           $res=$objreg->RegEmpres($check,$_POST['nickname'],md5($_POST['contraseña']),md5($_POST['pass']),$_POST['nom'],$_POST['pat'],$_POST['mat'],$_POST['curp'],$_POST['tel'],$_POST['correo'],$_POST['rfc']);
          #la vaidacion de los datos se hace en el RegUsu.php
          if($res==0)
            header("location:Registro.php?reg=0");
          else
            if($res==1)
              header("location:Registro.php?reg=1");
            else
              if($res==2)
                header("location:Registro.php?reg=2");
              else
                if($res==4)
                  header("location:Registro.php?reg=4");
                else
                  if($res==-1)// si la validacion del registro es -1 entonces procedemos a confirmar el registro
                    header("location:CargaFomularios.php");
          break;
        case 'Sociedad'://registramos sociedades de la misma manera que usuarios
          $check=$_POST['check'];
          $domicilio=$_POST['calle']." ".$_POST['noext']." ".$_POST['colonia']." ".$_POST['ciudad'];
          
          $res=$objreg->RegSoc($check,$_POST['empresa'],md5($_POST['titular']),md5($_POST['rfc']),$domicilio,md5($_POST['contraseña']),md5($_POST['pass']));
          #la vaidacion de los datos se hace en el RegUsu.php
          if($res==0)
            header("location:Registro.php?reg=0");
          else
            if($res==1)
              header("location:Registro.php?reg=1");
            else
              if($res==5)
                header("location:Registro.php?reg=5");
              else
                if($res==4)
                  header("location:Registro.php?reg=4");
                else
                  if($res==-1)// si la validacion del registro es -1 entonces procedemos a confirmar el registro
                    header("location:CargaFomularios.php");
          break;
      }
  }
}

 ?>

<h2 class="text-center text-dark">Registro</h2>
<div class="col-md-12">
<ul class="nav nav-tabs bg-success mt-3 rounded-pill" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active text-dark" id="normal-tab" data-toggle="tab" href="#normal" role="tab" aria-controls="normal" aria-selected="true">Sin Afiliación</a>
  </li>
  <li class="nav-item">
    <a class="nav-link text-dark" id="emprendedor-tab" data-toggle="tab" href="#emprendedor" role="tab" aria-controls="emprendedor" aria-selected="false">Emprendedor</a>
  </li>
  <li class="nav-item">
    <a class="nav-link text-dark" id="sociedad-tab" data-toggle="tab" href="#sociedad" role="tab" aria-controls="sociedad" aria-selected="false">Sociedad</a>
  </li>
  <li class="nav-item">
    <a class="nav-link text-dark" id="empresario-tab" data-toggle="tab" href="#empresario" role="tab" aria-controls="empresario" aria-selected="false">Empresario</a>
  </li>
</ul>


<div class="tab-content col-sm-8" id="myTabContent">
  <div class="tab-pane fade show active" id="normal" role="tabpanel" aria-labelledby="normal-tab">
  	<?php require_once("../Elementos/FormN.php"); ?>


  </div>
  <div class="tab-pane fade" id="emprendedor" role="tabpanel" aria-labelledby="emprendedor-tab">
    <?php require_once("../Elementos/FormEprende.php"); ?>
  </div>
  <div class="tab-pane fade" id="sociedad" role="tabpanel" aria-labelledby="sociedad-tab">
    <?php require_once("../Elementos/FormSoc.php"); ?>
  </div>
  <div class="tab-pane fade" id="empresario" role="tabpanel" aria-labelledby="empresario-tab">
    <?php require_once("../Elementos/FormEmp.php"); ?>
  </div>
</div>
</div>





<footer><?php require_once("Footer.php"); ?></footer>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
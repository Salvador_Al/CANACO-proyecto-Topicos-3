<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>Canaco</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <!--Open Iconic-->
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.0/font/css/open-iconic-bootstrap.min.css" />
</head>

<body>
    <div class="container main-container">
        <br>
        <h2 class="text-info"><span class="oi oi-people"></span>Sociedad</h2>
        <hr>
        <br>
        <div class="row">
            <div class="col-sm-7 pt-3 bg-info">
                <h4 class="text-white"><span class="oi oi-pencil"></span> Datos de la sociedad</h4>
                <hr>
                <form action="modSoci.php" method="post" name="formsoci">
                    <div class="form-group">
                        <input type="text" class="form-control" pattern="[A-Za-z]{80}" id="nombre" placeholder="Nombre">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" pattern="[A-Za-z]{40}" id="titu" placeholder="Titular">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="domi" placeholder="Domicilio">
                    </div>
                    <br>
                    <h4 class="text-white"><span class="oi oi-paperclip"></span> Cambiar Foto de Perfil</h4>
                    <hr>
                    <div class="form-group">
                        <input type="file" class="form-control-file text-white" name="fotoperfil" id="imgperfil">
                    </div>
                </form>
            </div>
            <div class="col-sm-4">
                <div class="card bg-info border-0" style="width: 25rem;">
                    <div class="card-header text-white">
                            <span class="oi oi-document"></span> Datos Actuales
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item" id="nomact">Nombre:</li>
                        <li class="list-group-item" id="appatact">Titular:</li>
                        <li class="list-group-item" id="apmatact">Domicilio:</li>
                        <li class="list-group-item" id="emailact">RFC:</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</body>
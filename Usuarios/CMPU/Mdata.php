<!DOCTYPE html>
<html lang="es">

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <link href="open-iconic-master/font/css/open-iconic-bootstrap.css" rel="stylesheet">
</head>

<body>
    <div class="container main-container pb-5">
        <br>
        <h2 class="text-info text-center"><span class="oi oi-person"></span> Usuario</h2>
        <hr>
        <br>
        <div class="row">
            <div class="col-sm-7 pt-3">
                <h4 class="text-info"><span class="oi oi-pencil"></span> Nickname</h4>
                <hr>
                <form action="">
                    <div class="form-group">
                        <input type="text" class="form-control" pattern="[A-Za-z]{20}" id="nikold"
                            placeholder="Nickname Actual">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" pattern="[A-Za-z]{20}" id="niknew"
                            placeholder="Nickname Nuevo">
                    </div>
                </form>


                <br>
                <h4 class="text-info"><span class="oi oi-pencil"></span> Datos Personales</h4>
                <hr>
                <form action="">
                    <div class="form-group">
                        <input type="text" class="form-control" pattern="[A-Za-z]{20}" id="nombre" placeholder="Nombre">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" pattern="[A-Za-z]{20}" id="appat"
                            placeholder="Apellido Paterno">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" pattern="[A-Za-z]{20}" id="apmat"
                            placeholder="Apellido Materno">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" pattern="[0-9]{10}" id="telef" placeholder="Telefono">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" id="email" placeholder="Correo">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="passant" placeholder="Contraseña Anterior">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="passnew" placeholder="Contraseña">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="passconf" placeholder="Confirmar Contraseña">
                    </div>
                    <br>
                    <h4 class="text-info"><span class="oi oi-paperclip"></span> Cambiar Foto de Perfil</h4>
                    <hr>
                    <div class="form-group">
                        <input type="file" class="form-control-file text-info" name="fotoperfil" id="imgperfil">
                    </div>
                </form>
            </div>
            <div class="col-sm-4">
                <div class="card bg-info border-0" style="width: 25rem;">
                    <div class="card-header text-white">
                        <span class="oi oi-person"></span> Datos Actuales
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item" id="nomact">Nombre:</li>
                        <li class="list-group-item" id="appatact">Apellido Paterno:</li>
                        <li class="list-group-item" id="apmatact">Apellido Materno:</li>
                        <li class="list-group-item" id="telact">Telefono:</li>
                        <li class="list-group-item" id="emailact">Correo:</li>
                        <li class="list-group-item" id="emailact">CURP:</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4"></div>
            <div class="col-4">
                <button type="button" class="btn btn-outline-success btn-lg btn-block">Guardar</button>
            </div>

        </div>
    </div>
</body>

</html>
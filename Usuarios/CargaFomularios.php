 
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">

<header><?php require_once("Nav.php"); ?></header>


<div class="jumbotron m-5 col-10">
  <h1 class="display-4" id="tit">Procesando... <span><img src="../CanacoIMG/carga.gif" width="150px" height="100px" alt="xx"></span></h1>
  <p class="lead" id="cam">Su Registro, esta siendo procesado</p>
  <hr class="my-4">
  <p>No cierre esta ventana, hasta que su registro sea validado.</p>
  <h2 class="text-center">Tiempo de solicitud <span id="contador"></span> seg...</h2>
</div>


<script>
  
    var num = 0;
    var tit=document.getElementById('tit');
    var cam=document.getElementById('cam');
    var contador = document.getElementById('contador');
    contador.innerHTML = num;
    var c;


      function conteo(){
      if (num==5)
      {
        cam.innerHTML="Ya puede cerrar esta ventana, o continuar navegando...";
        tit.innerHTML="!Finalizado¡";
        detener();
        
      }
      else
       contador.innerHTML = ++num;
    }
    c=setInterval(conteo, 1000);
    function detener()
    {
      clearInterval(c);
    }

    
</script>




<footer><?php require_once("Footer.php"); ?> </footer>
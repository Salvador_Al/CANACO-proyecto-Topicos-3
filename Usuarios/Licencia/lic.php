<link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
<header>
  <nav class="navbar navbar-light bg-info">
  <a class="navbar-brand" href="../<?php session_start(); echo "".$_SESSION['home']; ?> ">Cerrar
    <img src="../../CanacoIMG/icon/close.png" width="60" height="50" alt="">
  </a>
  </nav>
</header>


<div class="col-md-10 mx-auto">
  <h2>Solicitud de Licencias Municipales</h2>
<p class="text-justify m-5">Usted como Empresario, puede solicitar las credenciales especificas para las licencias correspondientes a las acciones que desempeñan sus empresas.</p>
<h4 class="mt-3">Acualmente usted a solicitado # credenciales:</h4>

<table class="table mt-5">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nombre de la Licencia</th>
      <th scope="col">Nombre del solicitante</th>
      <th scope="col">Estado</th>
      <th scope="col">Acción</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
    </tr>
  </tbody>
</table>
</div>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Canaco</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<body>
	<header><?php require_once("Elementos/Nav.php"); //Esta linea Carga el Nav ?></header>
    <br><br>
    <hr class="border border-dark">
    <div class="container">
        <h2>Red de apoyo emprendedor</h2>
        <img src="CanacoIMG\ina.jpg" alt="">
        <p>
        La Red de Apoyo al Emprendedor (RAE) es un programa de vinculación para los emprendedores y MiPyME’s, y diferentes instancias gubernamentales y el sector privado, instrumentado por el Instituto Nacional del Emprendedor (INADEM),  esto con la finalidad de trabajar en una estrategia que favorezca el crecimiento del sector emprendedor.
        </p><br>
        <p>En la Red de Apoyo al Emprendedor, el IMPI como aliado estratégico, promueve la cultura de protección de los derechos de propiedad industrial generados por emprendedores y MiPyME’s, a través de la vinculación y asesoramiento en los diferentes trámites y servicios que ofrece el IMPI.</p>
    </div>
    <hr class="border border-dark">
    <div class="container">
        <h2>Bolsa de trabajo</h2>
        <img src="CanacoIMG\bolsa.jpg" alt="">
        <h1>¡Trabaja en las mejores empresas de México!</h1><br>
        <p>
        Listado de empleos disponibles en la region
        </p>
    </div>
<hr class="border border-dark">

	<footer><?php require_once("Elementos/Footer.php"); // Esta Carga el Footer ?></footer>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	
</body>
</html>
<form action="../Usuarios/Registro.php" name="formulario" id="formulario" method="post">
	<input type="hidden" name="identificador" value="Usuarios">
	<input type="hidden" name="carga" value="1">
	<p class="h2">Usuario Sin Afiliación</p>
	<div class="d-flex mt-3">
				<div class="col-4">
					<p class="text-center bg-info form-control text-dark border-dark">Nickname</p>			
			    </div>
			    <div class="col-6"><input type="text" aria-label="Nickname" class="form-control" placeholder="Nickname" name="nickname"></div>
	</div>
			<div class="d-flex">
				<div class="col-4">
					<p class="text-center bg-info form-control text-dark border-dark">Contraseña</p>			
			    </div>
			    <div class="col-6"><input type="password" aria-label="password" class="form-control" name="contraseña"></div>
			</div>
			<div class="d-flex">
				<div class="col-4">
					<p class="text-center bg-info form-control text-dark border-dark">Repite Contraseña</p>			
			    </div>
			    <div class="col-6"><input type="password" aria-label="password" class="form-control" name="pass"></div>
			</div>
			<div class="d-flex">
				<div class="col-4">
					<p class="text-center bg-secondary form-control text-dark border-dark">Nombre y Apellidos</p>			
			    </div>
			    <div class="col-6 d-flex">		    	
			    	<input type="text" aria-label="First name" class="form-control" placeholder="Nombre" name="nom">
			  <input type="text" aria-label="Last name" class="form-control" placeholder="A. Paterno" name="pat">
			  <input type="text" aria-label="Last name" class="form-control" placeholder="A.Materno" name="mat">
			    </div>
			</div>

			<div class="d-flex">
				<div class="col-4">
					<p class="text-center bg-secondary form-control text-dark border-dark">Curp</p>			
			    </div>
			    <div class="col-6"><input type="text" aria-label="curp" class="form-control" name="curp"></div>
			</div>
			<div class="d-flex">
				<div class="col-4">
					<p class="text-center bg-secondary form-control text-dark border-dark">Correo</p>			
			    </div>
			    <div class="col-6"><input type="email" aria-label="email" class="form-control" name="correo"></div>
			</div>
			<div class="d-flex">
				<div class="col-4">
					<p class="text-center bg-secondary form-control text-dark border-dark">Telefono</p>			
			    </div>
			    <div class="col-6"><input type="text" aria-label="tel" class="form-control" name="tel"></div>
			</div>
			<div class="d-flex ml-3">
				<div class="form-check">
                    <input class="form-check-input" type="checkbox" value="1" id="defaultCheck1" name="check">
                        <label class="form-check-label" for="defaultCheck1">
                         Acepto <small><a href="../Usuarios/TerCondiciones.php">Los terminos y condiciones</a> de CANACO</small>
                       </label>
                 </div>
			</div>
			  <button type="button" class="btn btn-primary ml-3 mt-3" onclick="Redirigir();">Registrarme</button>
			</form>

	<script type="text/javascript">
		function Redirigir()
		{
			document.getElementById('formulario').submit()                                                                                                                                                            
		}
	</script>
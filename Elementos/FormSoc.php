
            <h1>Sociedad</h1>
            
            <form action="../Usuarios/Registro.php" name="formulariosoc" id="formulariosoc" method="post">
              <input type="hidden" name="identificador" value="Sociedad">
              <input type="hidden" name="carga" value="1">
              <div class="form-group">
                <div class="col-md-7">
                    <label for="nomemp" class="p-1 text-white card-body card bg-info col-md-12">Nombre De Empresa</label>
                    <input type="text" class="form-control" id="nomemp" name="empresa">
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-7">
                      <label for="titu" class="p-1 text-white card-body card bg-secondary col-2">Titular</label>
                      <input type="text" class="form-control" id="titu" name="titular">
                </div>
              </div>
              <div class="form-group ml-3">
                <div class="form-row"><label class="form-group">Contraseña</label><input type="password" name="contraseña" class="form-control"></div>
                <div class="form-row"><label class="form-group">Repetir Contraseña</label><input type="password" name="pass" class="form-control"></div>
              </div>
              <div class="form-group ml-3 mt-2">
                <div class="form-row">
                  <div class="col-md-3">
                      <label for="rfc" class="p-1 text-white card-body card bg-secondary">RFC de la empresa</label>
                      <input type="text" class="form-control" id="rfc" placeholder="Ejem: ASJ1403185L6" name="rfc">
                  </div>
                </div>
              </div>
              <!--Calle-->
              <div class="form-row ml-2">
                <div class="form-group col-md-3">
                  <label for="calle" class="p-1 text-white card-body card bg-secondary">Calle</label>
                  <input type="text" class="form-control" id="calle" name="calle">
                </div>
                <div class="form-group col-md-3">
                    <label for="numext" class="p-1 text-white card-body card bg-secondary"># Exterior</label>
                    <input type="text" class="form-control" id="numext" placeholder="#" name="noext">
                </div>
                <div class="form-group col-md-3">
                    <label for="colo" class="p-1 text-white card-body card bg-secondary">Colonia</label>
                    <input type="text" class="form-control" id="colo" name="colonia">
                </div>
                <div class="form-group col-md-3">
                    <label for="ciu" class="p-1 text-white card-body card bg-secondary">Ciudad / Municipio</label>
                    <input type="text" class="form-control" id="ciu" name="ciudad">
                </div>
              </div>
              <div class="d-flex ml-3">
        <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="1" id="defaultCheck1" name="check">
                        <label class="form-check-label" for="defaultCheck1">
                         Acepto <small><a href="TerCondiciones.php">Los terminos y condiciones de CANACO</a></small>
                       </label>
                 </div>
      </div>
              <button type="button" class="btn btn-primary ml-3 mt-3" onclick="RedirigirSoc();">Registrarme</button>
      </form>

  <script type="text/javascript">
    function RedirigirSoc()
    {
      document.getElementById('formulariosoc').submit()                                                                                                                                                            
    }
  </script>
             
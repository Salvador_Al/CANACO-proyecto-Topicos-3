<ul class="navbar-nav mr-auto">
    <li class="nav-item dropdown">
      <a class="nav-link text-white" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Usuarios</a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="#">Modificacion de usuarios</a>
        <a class="dropdown-item" href="#">Eliminacion de usuarios</a>
      </div>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link text-white" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Tramites</a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="#">Modificacion de tramites</a>
        <a class="dropdown-item" href="#">Eliminacion de tramites</a>
      </div>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link text-white" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Noticias</a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="#">Publicacion de noticias</a>
        <a class="dropdown-item" href="#">Modificacion de noticias</a>
        <a class="dropdown-item" href="#">Eliminacion de noticias</a>
      </div>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link text-white" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Convocatorias y noticias canaco</a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="#">Publicacion de noticias</a>
        <a class="dropdown-item" href="#">Modificacion de noticias</a>
        <a class="dropdown-item" href="#">Eliminacion de noticias</a>
        <a class="dropdown-item" href="#">Publicacion de Convocatorias</a>
        <a class="dropdown-item" href="#">Modificacion de Convocatorias</a>
        <a class="dropdown-item" href="#">Eliminacion de Convocatorias</a>
      </div>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link text-white" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Talleres</a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="#">Publicacion de Talleres</a>
        <a class="dropdown-item" href="#">Modificacion de Talleres</a>
        <a class="dropdown-item" href="#">Eliminacion de Talleres</a>
      </div>
    </li>
</ul>
<div class="m-0">
  <nav class="navbar navbar-expand-lg bg-info">
  <a class="navbar-brand mr-3 text-white" href="../index.php"><img src="../CanacoIMG/icon/icon-Canaco.png" height="50px" width="50px" ></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item m-">
        <a class="nav-link text-white" href="../index.php"><img src="../CanacoIMG/icon/icon-Canaco.png" alt="Inicio" height="25px" width="25px">  Inicio</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white" href="../nosotros.php"><img src="../CanacoIMG/icon/icon-Us.png" alt="Inicio" height="30px" width="30px"> Nosotros</a>
      </li>
      <li class="nav-item text-white">
        <a class="nav-link text-white" href="../Usuarios/Registro.php"><img src="../CanacoIMG/icon/icon-affiliate2.png" alt="Inicio" height="30px" width="30px"> Afiliate</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white" href="../servicios.php" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../CanacoIMG/icon/icon-service.png" alt="Inicio" height="30px" width="30px">
          Servicios
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="../servicios.php">Servicios</a>
           <div class="dropdown-divider border border-dark"></div>
          <a class="dropdown-item" href="../bolsatrabajo.php">Bolsa de Trabajo</a>
          <a class="dropdown-item" href="../redemprendedor.php">Red del Emprendedor</a>
        </div>
      </li>      
      <li class="nav-item">
        <a class="nav-link text-white" href="../galeria.php"><img src="../CanacoIMG/icon/icon-gallery.png" alt="Inicio" height="30px" width="30px"> Galeria</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white" href="../contacto.php"><img src="../CanacoIMG/icon/icon-contact.png" alt="Inicio" height="30px" width="30px"> Contacto</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0 mr-sm-2 name="busqueda" method="get" action="../Resultados.php">
      <input class="form-control mr-sm-2" type="search" placeholder="Buscar" aria-label="Search" name="search">
      <button class="btn btn-secondary border border-dark" type="submit">Buscar</button>
    </form>
    <form class="form-inline my-2 my-lg-0 mr-sm-2">
      <a class="btn btn-primary mr-sm-2 border border-dark" href="../Usuarios/Sesion.php">Iniciar Sesión</a>
      <a type="button" class="btn btn-secondary border border-dark" href="../Usuarios/Registro.php">Registro</a>
    </form>
  </div>
</nav>
  
</div>
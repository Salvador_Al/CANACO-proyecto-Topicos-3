
        <h1>Empresario</h1>
        <hr>
        <br>
        <form name="formempres" id="formempres" action="../Usuarios/Registro.php" method="post"> 
          <input type="hidden" name="carga" value="1">
          <input type="hidden" name="identificador" value="Empresario">
          <!--Nickname-->
          <div class="form-row mb-2">
            <div class="col-md-3">
              <label for="nickname" class="p-1 text-white card-body card bg-info">Nombre de usuario</label>
              <input type="text" class="form-control" name="nickname">
            </div>
          </div>
            <!--Usuario-->
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-4">
                  <label for="correo" class="p-1 text-white card-body card bg-info">Email</label>
                  <input type="text" class="form-control"  name="correo">  
                </div>
                <div class="col-md-3">
                  <label for="contraseña" class="p-1 text-white card-body card bg-info">Contraseña</label>
                  <input type="password" name="contraseña" class="form-control">
                </div>
                <div class="col-md-3">
                  <label for="pass" class="p-1 text-white card-body card bg-info">Confirmar Contraseña</label>
                  <input type="password" name="pass" class="form-control">
                </div>
              </div>
            </div>
          <!--Nombre del Empresario-->
          <label class="p-1 text-white card-body card bg-secondary col-md-4">Nombre del Empresario</label>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-4">
                <input type="text" name="nom" class="form-control"  placeholder="Nombre">  
              </div>
              <div class="col-md-3">
                <input type="text" name="pat" class="form-control"  placeholder="Apellido Paterno">
              </div>
              <div class="col-md-3">
                <input type="text" name="mat" class="form-control" placeholder="Apellido Materno">
              </div>
            </div>
          </div>
          <!--Telefono y Correo-->
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-3">
                  <label for="curp" class="p-1 text-white card-body card bg-secondary col-3">CURP</label>
                  <input type="text" name="curp" class="form-control"  placeholder="Ejem: SASO750909HDFNNS05">
              </div>
              <div class="col-md-3">
                  <label for="rfc" class="p-1 text-white card-body card bg-secondary col-3">RFC</label>
                  <input type="text" name="rfc" class="form-control"  placeholder="Ejem: RICA800925PL7">
              </div>
              <div class="col-md-3">
                  <label for="tel" class="p-1 text-white card-body card bg-secondary">Telefono (10 digitos)</label>
                  <input type="text" name="tel" class="form-control"  placeholder="Ejem: 3534786688">
              </div>
            </div>
          </div>
          <div class="d-flex ml-3">
        
                    <input type="checkbox" class="form-check-input" name="mm" value="1" id="mm" >
                    <label class="form-check-label" for="mm">
                         Acepto <small><a href="../Usuarios/TerCondiciones.php">Los terminos y condiciones</a> de CANACO</small>
                    </label>
          </div>
          <div class="form-group">
            <button type="button" class="btn btn-primary mt-3" onclick="Redirigirempre();">Registrarme</button>
          </div>

      </form>

  <script type="text/javascript">
    function Redirigirempre()
    {
      document.getElementById('formempres').submit()                                                                                                                                                            
    }
  </script>
      
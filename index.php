<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Canaco</title>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<body>
	<header><?php require_once("Elementos/Nav.php"); //Esta linea Carga el Nav ?></header>

	
    <div id="carouselExampleControls" class="carousel slide mt-5 mb-3" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="CanacoIMG/rifa.jpeg" class="d-block mx-auto" width="800px" height="400px" alt="Nachos">
    </div>
    <div class="carousel-item">
      <img src="CanacoIMG/promo.jpeg" class="d-block mx-auto" width="800px" height="400px" alt="Nachos">
    </div>    
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon bg-dark" aria-hidden="true"></span>
    <span class="sr-only">Anterior</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon bg-dark" aria-hidden="true"></span>
    <span class="sr-only">Siguiente</span>
  </a>
</div>

<h1 class="text-center text-info mt-5">CANACO</h1>

<div class="container">

<div class="col-md-12 d-flex mt-5">
  <div class="col-md-4 border border-dark bg-light">
    <h3 class="text-center">Noticias</h3>
    <hr class="border border-dark">
    <div class="d-flex justify-content-center">

      <div class="card d-flex justify-content-center border border-dark" style="width: 18rem;">
      <img src="CanacoIMG/expohogar.jpeg" class="card-img-top" alt="NOOOUU">
    <div class="card-body">
    <h5 class="card-title">Expo Hogar 2019</h5>
    <p class="card-text">Obten descuentos en las sucursales afiliadas de hasta de un 15%.
    <span><a href="galeria.php">Ver Más</a></span> </p>
    </div>
    </div>

    </div>
    <hr class="border border-dark"> 
    
  </div>
  <div class="col-md-8">
    <h2 class="text-center text-info">Relevantes</h2>
    <hr class="border border-dark"> 
    <div class="d-flex justify-content-center">
      
      <div class="card border border-dark col-md-6">
          <img src="CanacoIMG/entrega.jpeg" class="card-img-top" height="200px" alt="...">
          <div class="card-body">
              <h5 class="card-title">Entregas de Afiliaciones</h5>
              <p class="card-text">Descubre a los nuevos emprendedores que estan disfrutando ya los beneficiose de afiliarse a la CANACO <span> <a href="galeria.php">Ver mas...</a>  </span></p>
              
          </div>
      </div>
      <div class="card border border-dark ml-2 col-md-6">
          <img src="CanacoIMG/fondonacional.jpeg" class="card-img-top" height="200px" alt="...">
          <div class="card-body">
              <h5 class="card-title">Fondo Nacional</h5>
              <p class="card-text">Descubre las ventajas de afiliarte al fondo nacional del emprendedor disfrutando ya los beneficiose de afiliarse a la CANACO <span> <a href="galeria.php">Ver mas...</a>  </span></p>
              
          </div>
      </div>
      
    </div>
    <div class="d-flex justify-content-center mt-2">
      <div class="card border border-dark col-md-6">
          <img src="CanacoIMG/afiliacion.jpeg" class="card-img-top" height="200px" alt="...">
          <div class="card-body">
              <h5 class="card-title">Afiliate a la Camara de Comercio</h5>
              <p class="card-text">Descubre las ventajas de afiliarte a la camara de comercio disfrutando ya los beneficiose de afiliarse a la CANACO <span> <a href="afiliate.php">Afiliate</a>  </span></p>
              
          </div>
      </div>
      <div class="card border border-dark ml-2 col-md-6">
          <img src="CanacoIMG/expohogar.jpeg" class="card-img-top" height="200px" alt="...">
          <div class="card-body">
              <h5 class="card-title">Expohogar</h5>
              <p class="card-text">Se realizara por un año más la Expo hogar, donde podras recibir hasta un 15 % de descuento en tiendas afiliadas a la CANACO.  <span> <a href="galeria.php">Ver mas...</a>  </span></p>
              
          </div>
      </div>
    </div>
  </div>
</div>

<hr class="border border-dark">




  </div>











	<footer><?php require_once("Elementos/Footer.php"); // Esta Carga el Footer ?></footer>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	
</body>
</html>
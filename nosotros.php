<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">


<header><?php require_once("Elementos/Nav.php"); ?></header>

    <?php //Aqui va el codigo HTML o php que quieran poner ?>
    <br>
    <br>
    <div class="container main-container">
        <h1>NOSOTROS</h1>
        <hr>
        <div class="text-center m-3">
            <img src="CanacoIMG/concanaco-shangai.jpg" class="shadow-sm p-3 mb-5 bg-white rounded" alt="concanaco-shangai">
        </div>
        <div class="row">
            <div class="card border-secondary mb-3 shadow-sm p-3 mb-5 bg-white rounded" style="max-width: 42rem;">
                <div class="card-body text-secondary text-center">
                    <p class="card-text justify-content-center">La Cámara Nacional de Comercio, Servicio y Turismo de Morelia, es una Institución de interés público, autónoma, con personalidad jurídica y patrimonio propio constituido, conforme a lo dispuesto en la Ley de Cámaras Empresariales y sus Confederaciones.</p>
                </div>
            </div>
            <div class="row col-5 ml-sm-2">
                    <div class="card text-white mb-3" style="max-width: 30rem; background-color: #8D8D8D;">
                        <div class="card-body">
                            <p class="card-text">La Canaco SERVyTUR Morelia, representa, promueve y defiende nacional e internacionalmente las actividades del comercio, los servicios y el turismo; y colabora con los tres niveles de Gobierno, para lograr un mejor crecimiento socioeconómico y la generación de riqueza en nuestro país.</p>
                        </div>
                    </div>
                <div class="card text-white mb-3" style="max-width: 30rem; background-color: #8D8D8D;">
                        <div class="card-body">
                            <p class="card-text">Dentro de la circunscripción de CANACO SERVyTUR MORELIA están los siguientes municipios del estado de Michoacán.</p>
                        </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="card text-white mb-3" style="max-width: 20rem; background-color: #8D8D8D;">
                <div class="card-body">
                        <p class="card-text">Contactanos</p>
                        <p>
                            <a href="https://www.facebook.com/CanacoSahuayo"><img src="CanacoIMG/icon/icon-facebook.png" class="m-sm-1" style="width: 30px; height: 30px;" alt="Facebook"></a>
                            <a href="https://twitter.com/CanacoSahuayo"><img src="CanacoIMG/icon/icon-twitter.png" class="m-sm-1" style="width: 30px; height: 30px;" alt="twitter"></a>
                            <a href="https://www.youtube.com/channel/UC8JsCEEaOsBkXMG7wtHPVkA"><img src="CanacoIMG/icon/icon-youtube.png" class="m-sm-1" style="width: 30px; height: 30px;" alt="Youtube"></a>
                        </p>
                        <p class="card-text">Ubicados en</p>
                        <p class="card-text">Calle Constitucion y Madero Int. 18 (8.07 km) 59000 Sahuayo de Morelos</p>
                </div>
            </div>
            <div class="ml-sm-2 mt-xl-4" >
                <img src="CanacoIMG/headerConcanaco.png" style="max-width: 50rem;" class="shadow-sm p-3 mb-5 bg-white rounded" alt="headerConcanaco">
            </div>
        </div>
        <br>
        <br>
        <h2>Valores</h2>
        <br>
        <div class="row">
            <div class="pt-4">
                <h5>Misión</h5>
                <div class="card-body" style="max-width: 40rem;">
                    <blockquote class="blockquote mb-0">
                        <p>Representar, defender y promover los intereses legítimos de las empresas
                            pertenecientes a los sectores Comercio, Servicios y Turismo,</p>
                        <footer class="blockquote-footer"> a través de la
                            integración y fortalecimiento de todas las Cámaras Confederadas.</footer>
                    </blockquote>
                </div>
            </div>
            <div>
                <img src="CanacoIMG/concanaco-shangai.jpg" class="shadow-sm p-3 mb-5 bg-white rounded" alt="concanaco-shangai" style="max-width: 25rem;">
            </div>
        </div>
        <br>
        <div class="row">
            <div>
                <img src="CanacoIMG/cifras-septiembre-2019.jpg" class="shadow-sm p-3 mb-5 bg-white rounded" alt="concanaco-shangai" style="max-width: 30rem;">
            </div>
            <div class="justify-content-end ml-3 pt-2">
                <h5>Visión</h5>
                <div class="card-body" style="max-width: 40rem;">
                    <blockquote class="blockquote mb-0">
                        <p>Ser la institución líder de los organismos cúpula de la iniciativa privada,
                            participando activamente en beneficio de las empresas y cámaras
                            pertenecientes a los sectores:</p>
                        <footer class="blockquote-footer"> Comercio, Servicios y Turismo de nuestro país,
                                    generando riqueza con responsabilidad social.</footer>
                    </blockquote>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div>
            <h5>Objetivos</h5>
                <div class="card-body" style="max-width: 40rem;">
                    <ul>
                        <li>Coadyuvar a la unión y desarrollo de las Cámaras Confederadas.</li>
                        <li>Fomentar la eficacia competitiva de los establecimientos de Comercio, Servicios y Turismo.</li>
                        <li>Fortalecer la imagen de los sectores Comercio, Servicios y Turismo.</li>
                        <li>Promover el sano desarrollo de los negocios, procurando elevar laética empresarial.</li>
                        <li>Establecer relaciones de colaboración con instituciones afines,nacionales y extranjeras.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
<footer><?php require_once("Elementos/Footer.php"); ?> </footer>
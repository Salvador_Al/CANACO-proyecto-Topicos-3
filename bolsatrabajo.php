<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Canaco</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<body>
	<header><?php require_once("Elementos/Nav.php"); //Esta linea Carga el Nav ?></header>
    <br><br>
<div class="container">
    <div class="card mb-3">
        <img src="CanacoIMG/cananco.png" class="card-img-top" height="300px" alt="...">
        <div class="card-body">
            <h5 class="card-title">Comprador de insumos</h5>
            <p class="card-text">Empresa manufacturera dedicados al desarrollo, innovación y comercialización de nuevos productos</p>
            <p class="card-text">¡Por expansión buscamos al mejor talento! Solicitamos Comprador Internacional... </p>
            <div class="col-md-9 text-center">
                <a href="https://www.occ.com.mx/empleo/oferta/12457215-comprador-de-insumos?ai=false&origin=unknown&page=1&rank=1&returnURL=%2Fempleos%2Fen-michoacan%2Fen-la-ciudad-de-jiquilpan%2F%231&sessionid=b104e55d-fb98-4c35-a051-9a233c09fdd6&showseo=true&type=0&userid=&uuid=df9e81c8-0c2b-4155-91d1-a1503a250fde"><button type="submit" class="btn btn-primary btn-lg">Ver mas</button></a>
            </div>
        </div>
    </div>
    <div class="card">
        <img src="CanacoIMG/Canranco.png" class="card-img-top" height="300px" alt="...">
        <div class="card-body">
            <h5 class="card-title">ENCARGADO DE CONTABILIDAD</h5>
            <p class="card-text">Empresa líder en la elaboración de lácteos, solicita:</p>
            <p class="card-text">ESPECIALISTA EN COSTOS</small></p>
            <div class="col-md-9 text-center">
                <a href="https://www.occ.com.mx/empleo/oferta/12394539-encargado-de-contabilidad?ai=false&origin=unknown&page=1&rank=3&returnURL=%2Fempleos%2Fen-michoacan%2Fen-la-ciudad-de-jiquilpan%2F%233&sessionid=b104e55d-fb98-4c35-a051-9a233c09fdd6&showseo=true&type=1&userid=&uuid=df9e81c8-0c2b-4155-91d1-a1503a250fde"><button type="submit" class="btn btn-primary btn-lg">Ver mas</button></a>
            </div>
        </div>
    </div>
</div>
    
<hr class="border border-dark">

	<footer><?php require_once("Elementos/Footer.php"); // Esta Carga el Footer ?></footer>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	
</body>
</html>